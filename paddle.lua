-- paddle.lua
local lg = love.graphics
Paddle = {}
Paddle.__index = Paddle

function Paddle.init(x, y, color, upButton, downButton)
  local self = setmetatable({}, Paddle)
  self.x = x
  self.y = y
  self.width = 20
  self.height = 80
  self.hw = self.width / 2
  self.hh = self.height / 2
  self.color = color or colors.gray

  self.speed = 200

  self.upButton = upButton
  self.downButton = downButton

  return self
end

function Paddle:draw()
  lg.setColor(self.color)
  lg.rectangle('fill', self.x, self.y, self.width, self.height)
end

function Paddle:update(dt)
  -- handle input
  if love.keyboard.isDown(self.upButton)  then
    self.y = self.y - self.speed * dt
  elseif love.keyboard.isDown(self.downButton) then
    self.y = self.y + self.speed * dt
  end
  -- clamp values
  self.y = math.clamp(0, self.y, lg.getHeight() - self.height)
end

-- util
function math.clamp(low, n, high)
  return math.min(math.max(low, n), high)
end
