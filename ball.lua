-- ball.lua
local lg = love.graphics
Ball = {}
Ball.__index = Ball

function Ball.init(x, y, width, height, color, gamemanager)
  local self = setmetatable({}, Ball)
  self.width = width or 20
  self.height = height or 20
  self.hw = self.width / 2
  self.hh = self.height / 2

  self.x = x or lg.getWidth() / 2 - self.hw
  self.y = y or lg.getHeight() / 2 - self.hh

  self.color = color or colors.gray

  self.speed = 200
  self.vx = 0
  self.vy = 0
  self.spawnDelay = 1
  self.gm = gamemanager
  self.canScore = true
  self:setRandomDirection()

  return self
end

function Ball:draw()
  lg.setColor(self.color)
  lg.circle('fill', self.x + self.hw, self.y + self.hh, self.hh, 20)
end

function Ball:update(dt, p1, p2)
  if self.spawnDelay > 0 then
    self.spawnDelay = self.spawnDelay - dt

  elseif self.canScore then
    self:move(dt, p1, p2)
  end
end

-- movement -----------------------------------------------
function Ball:move(dt, p1, p2)
  local tx = self.x + self.vx * dt
  local ty = self.y + self.vy * dt

  -- collision with side walls (scoring) ------------------
  if tx + self.width > lg.getWidth() then
    -- player 1 scores
    self.vx = 0
    self.vy = 0
    self.gm:score(1)
    self.canScore = false

  elseif tx < 0 then
    -- player 2 scores
    self.vx = 0
    self.vy = 0
    self.gm:score(2)
    self.canScore = false
  end

  -- bouncing ---------------------------------------------
  if ty + self.height > lg.getHeight() then
    ty = lg.getHeight() - self.height
    self.vy = -self.vy
  elseif ty < 0 then
    ty = 0
    self.vy = -self.vy
  end

  -- collision with paddles -------------------------------
  if (tx <= p1.x + p1.width) and
  (ty >= p1.y and ty + self.height <= p1.y + p1.height) then
      -- paddle 1 is the left paddle, so the ball collides with its
      -- rightmost edge
      tx = p1.x + p1.width
      self.vx = -self.vx
      --self.vy = -- todo: distance from paddle center * something
  elseif (tx + self.width >= p2.x) and
  (ty >= p2.y and ty + self.height <= p2.y + p2.height) then
      tx = p2.x - self.width
      self.vx = -self.vx
  end



  self.x = tx
  self.y = ty
end

function Ball:setRandomDirection()
  local dir = love.math.random(1, 360)
  self.vx = self.speed * math.cos(dir)
  self.vy = self.speed * math.sin(dir)
end
