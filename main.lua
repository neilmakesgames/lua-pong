require 'colors'
require 'gamemanager'

function love.load(arg)
  love.graphics.setBackgroundColor(colors.bg)
  gamemanager:init()
end

function love.update(dt)
  gamemanager:update(dt)
end

function love.draw()
  gamemanager:draw()
end

function love.keypressed(key)
  if key == 'r' then
    gamemanager:reset()
  elseif key == 'escape' then
    love.event.quit()
  end
end
