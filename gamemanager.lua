-- gamemanager.lua
require 'ball'
require 'paddle'
local lg = love.graphics
gamemanager = {}

-- init and reset -----------------------------------------
function gamemanager:init()
  self.font = lg.newFont(60)
  self.canvas = lg.newCanvas(lg.getWidth(), lg.getHeight())
  self:reset()
end

function gamemanager:reset()
  local y = lg.getHeight() / 2 - 40;
  self:newBall()

  self.p1 = Paddle.init(                20, y, colors.blue, 'w', 's')
  self.p2 = Paddle.init(lg.getWidth() - 40, y, colors.red, 'up', 'down')

  self.p1score = 0
  self.p2score = 0
  self.deathDelay = nil
  self:cacheCanvas()
end

function gamemanager:newBall()
  --(x, y, width, height, color, gamemanager)
  self.ball = Ball.init(
    lg.getWidth() / 2 - 10,
    lg.getHeight() / 2 - 10,
    20,
    20,
    colors.gray,
    self)
end

-- drawing ------------------------------------------------
function gamemanager:draw()
  -- draw background
  if self.canvasDirty then
    self:cacheCanvas()
  end
  lg.setColor(colors.gray)
  lg.draw(self.canvas, 0, 0)

  -- draw ball and paddles
  self.ball:draw()
  self.p1:draw()
  self.p2:draw()
end

function gamemanager:cacheCanvas()
  local w = lg.getWidth()
  lg.setCanvas(self.canvas)
  lg.clear()
  -- draw scores
  lg.setFont(self.font)
  lg.setColor(colors.gray)
  lg.printf(self.p1score,      0, 20, w * .5, 'center')
  lg.printf(self.p2score, w * .5, 20, w * .5, 'center')
  -- draw dotted line
  lg.setLineWidth(2)
  for y = 0, lg.getHeight(), 20 do
    lg.line(w * .5, y, w * .5, y+10)
  end
  lg.setCanvas()
  self.canvasDirty = false
end

-- updating -----------------------------------------------

function gamemanager:update(dt)
  self.ball:update(dt, self.p1, self.p2)
  self.p1:update(dt)
  self.p2:update(dt)

  -- spawn new ball 1 second after scoring:
  if self.deathDelay ~= nil then
    self.deathDelay = self.deathDelay - dt
    if self.deathDelay <= 0 then
      self:spawnNewBall()
    end
  end

end

-- scoring ------------------------------------------------

function gamemanager:score(playerNumber)

  if self.ball.canScore and playerNumber == 1 then
    self.p1score = self.p1score + 1
    self.deathDelay = 1
    self.canvasDirty = true
  elseif self.ball.canScore and playerNumber == 2 then
    self.p2score = self.p2score + 1
    self.deathDelay = 1
    self.canvasDirty = true
  end
end

function gamemanager:spawnNewBall()
  self:newBall()
  self.deathDelay = nil
end
